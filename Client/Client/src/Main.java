import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        try (Socket server = new Socket("127.0.0.1", 7654)) {
            System.out.println("Connected to server.");
            OutputStream out = server.getOutputStream();
            InputStream in = server.getInputStream();
            String message = "";
            byte[] buffer = new byte[2048];
            while (!message.equals("over")) {
                message = new Scanner(System.in).nextLine();
                out.write(message.getBytes());
                int read = in.read(buffer);
                String rcv = new String(buffer, 0, read);
                System.out.println("\n" + rcv );
            }
//            String[] messages = {"salam", "chetori?", "che-khabar?"};
//            for (String msg: messages) {
//                out.write(msg.getBytes());
//                System.out.println("SENT: " + msg);
//                int read = in.read(buffer);
//                System.out.println("RECV: " + new String(buffer, 0, read));
//            }
//            System.out.print("All messages sent.\nClosing ... ");
        } catch (IOException ex) {
            System.err.println(ex);
        }
        System.out.println("done.");
    }
}
