import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class ClientHandler implements Runnable {

    private Socket client;

    public ClientHandler(Socket client) {
        this.client = client;
    }

    @Override
    public void run() {
        try {
            StringBuilder builder = new StringBuilder();
            OutputStream out = client.getOutputStream();
            InputStream in = client.getInputStream();
            String message = "";
            while (true) {
                byte[] buffer = new byte[2048];
                int read = in.read(buffer);
                message = new String(buffer,0, read);
                builder.append(message + "\n");
                String all = builder.toString();
                System.out.println(all);
                out.write(all.getBytes());
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.close();
            } catch (IOException ex) {
                System.err.println(ex);
            }
        }
    }
}

